import { Meteor } from 'meteor/meteor';
import StlThumbnailer from 'node-stl-thumbnailer';

/**
 * Transform model into buffer
 *
 * @param thumbnail
 * @returns {Promise}
 */
const getBuffer = (thumbnail) => {
  return new Promise((resolve, reject) => {
    thumbnail.toBuffer((err, buffer) => {
      if (err) return reject(err);
      resolve(buffer);
    });
  });
};

/**
 * Get model thumbnail
 *
 * @param url {string} - url to remote thumbnail
 * @param width {number} - thumbnail width
 * @param height {number} - thumbnail height
 * @returns {Promise.<string>} - base64 encoded thumbnail
 */
export const getModelThumbnail = async (url, width = 500, height = 500) => {
  try {
    const thumbnails = await new StlThumbnailer({
      requestThumbnails: [{ width, height, }],
      url,
    });
    const buffer = await getBuffer(thumbnails[0]);
    return buffer.toString('base64');
  } catch (err) {
    throw new Meteor.Error('Failed to make thumbnail');
  }
};