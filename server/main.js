import { Meteor } from 'meteor/meteor';
import Consumer from '/imports/api/kafka/consumer';

Meteor.startup(() => {
  //http://www.instructables.com/files/orig/F0Q/U1DI/IY4Q5LSH/F0QU1DIIY4Q5LSH.stl
  const consumer = new Consumer();
  consumer.listen();
});
