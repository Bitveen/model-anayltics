import { Meteor } from 'meteor/meteor';
import Kafka from 'node-rdkafka';
import { Part } from 'meteor:dfam-parts';
import { getModelInfo } from '../models/info';
import { getModelThumbnail } from '../models/thumbnails';

/**
 * Listen for messages
 */
export default class Consumer {
  constructor() {
    this.consumer = new Kafka.KafkaConsumer({
      'group.id': Meteor.settings.kafka.groupId,
      'metadata.broker.list': Meteor.settings.kafka.broker,
    }, {});
  }

  listen() {
    this.consumer.connect();
    this.consumer.on('ready', () => {
      this.consumer.subscribe(Meteor.settings.kafka.topics);
    });
    this.consumer.on('data', Consumer.handleMessage);
  }

  static async handleMessage(data) {
    const url = data.message.toString();
    try {
      const results = await Promise.all([getModelThumbnail(url), getModelInfo(url)]);
      const part = Part.find({ });
      part.thumbnail_as_submitted = results[0];
      part.geometry_analytics = results[1];
      part.save();
    } catch (err) {
      Logger.error(err);
    }
  }

}