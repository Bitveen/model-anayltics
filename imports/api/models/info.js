import { Meteor } from 'meteor/meteor';
import NodeStl from 'node-stl';
import request from 'request-promise';

/**
 * Fetch model info
 *
 * @param url
 * @returns {Promise.<{volume: *, weight: *, boundingBox: {x: *, y: *, z: *}}>}
 */
export const getModelInfo = async (url) => {
  try {
    const buffer = await request.get(url, { encoding: null });
    const stl = new NodeStl(buffer);
    return {
      volume: stl.volume,
      weight: stl.weight,
      boundingBox: {
        x: stl.boundingBox[0],
        y: stl.boundingBox[1],
        z: stl.boundingBox[2],
      }
    };
  } catch (err) {
    throw new Meteor.Error('Failed to get model info');
  }
};